/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tableModels;

import java.sql.Date;

/**
 *
 * @author bakfunk
 */
public class Mitarbeiter {

    private String Personalnummer;
    private String Vorname;
    private String Name;
    private Date GebDat;
    private int Fk_Abteilung;

    public Mitarbeiter() {
    }

    public Mitarbeiter(String Personalnummer, String Vorname, String Name, Date GebDat, int Fk_Abteilung) {
        this.Personalnummer = Personalnummer;
        this.Vorname = Vorname;
        this.Name = Name;
        this.GebDat = GebDat;
        this.Fk_Abteilung = Fk_Abteilung;
    }
    
    public Object[] getObjectArray() {
        Object[] data = new Object[] {
            Personalnummer,
            Vorname,
            Name,
            GebDat,
            Fk_Abteilung
        };
        
        return data;
    }

    public String getPersonalnummer() {
        return Personalnummer;
    }

    public void setPersonalnummer(String Personalnummer) {
        this.Personalnummer = Personalnummer;
    }

    
     
    public String getVorname() {
        return Vorname;
    }

    public void setVorname(String Vorname) {
        this.Vorname = Vorname;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public Date getGebDat() {
        return GebDat;
    }

    public void setGebDat(Date GebDat) {
        this.GebDat = GebDat;
    }

    public int getFk_Abteilung() {
        return Fk_Abteilung;
    }

    public void setFk_Abteilung(int Fk_Abteilung) {
        this.Fk_Abteilung = Fk_Abteilung;
    }

}
