/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tableModels;

/**
 *
 * @author TagmouzSa
 */
public class Artikel {
    
      private String aNr;
      private String Name;
  private String beschreibung;
  
  private double preis;
  private double nettopreis;
  private int menge;
  private int Biosiegel;
  private int Steuersatz;
  private int VE;
  private long EANNummer;
  private int fairtrade;
  private int Fk_Erzeuger;
  private int Fk_Artikelgruppe;

  public Artikel(){
  }

    public Artikel(String aNr, String Name, String beschreibung, double preis, double nettopreis, int menge, int Biosiegel, int Steuersatz, int VE, long EANNummer, int fairtrade, int Fk_Erzeuger, int Fk_Artikelgruppe) {
        this.aNr = aNr;
        this.Name = Name;
        this.beschreibung = beschreibung;
        this.preis = preis;
        this.nettopreis = nettopreis;
        this.menge = menge;
        this.Biosiegel = Biosiegel;
        this.Steuersatz = Steuersatz;
        this.VE = VE;
        this.EANNummer = EANNummer;
        this.fairtrade = fairtrade;
        this.Fk_Erzeuger = Fk_Erzeuger;
        this.Fk_Artikelgruppe = Fk_Artikelgruppe;
    }
    
    public Object[] getObjectArray() {
        Object[] data = new Object[] {
            aNr,
            Name,
            beschreibung,
            preis,
            nettopreis,
            menge,
            Biosiegel,
            Steuersatz,
            VE,
            EANNummer,
            fairtrade,
            Fk_Erzeuger,
            Fk_Artikelgruppe
        };
        
        return data;
    }

    public String getaNr() {
        return aNr;
    }

    public void setaNr(String aNr) {
        this.aNr = aNr;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public double getPreis() {
        return preis;
    }

    public void setPreis(double preis) {
        this.preis = preis;
    }

    public double getNettopreis() {
        return nettopreis;
    }

    public void setNettopreis(double nettopreis) {
        this.nettopreis = nettopreis;
    }

    public int getMenge() {
        return menge;
    }

    public void setMenge(int menge) {
        this.menge = menge;
    }

    public int getBiosiegel() {
        return Biosiegel;
    }

    public void setBiosiegel(int Biosiegel) {
        this.Biosiegel = Biosiegel;
    }

    public int getSteuersatz() {
        return Steuersatz;
    }

    public void setSteuersatz(int Steuersatz) {
        this.Steuersatz = Steuersatz;
    }

    public int getVE() {
        return VE;
    }

    public void setVE(int VE) {
        this.VE = VE;
    }
    
    public long getEANNummer() {
        return EANNummer;
    }

    public void setEANNummer(long EANNummer) {
        this.EANNummer = EANNummer;
    }

    public int getFairtrade() {
        return fairtrade;
    }

    public void setFairtrade(int fairtrade) {
        this.fairtrade = fairtrade;
    }

    public int getFk_Artikelgruppe() {
        return Fk_Artikelgruppe;
    }
    
    public void setFk_Erzeuger(int Fk_Erzeuger) {
        this.Fk_Erzeuger = Fk_Erzeuger;
    }

    public void setFk_Artikelgruppe(int Fk_Artikelgruppe) {
        this.Fk_Artikelgruppe = Fk_Artikelgruppe;
    }
    
     public int getFk_Erzeuger() {
        return Fk_Erzeuger;
    }
    

    
    
    

    

 

  
  

    
}
