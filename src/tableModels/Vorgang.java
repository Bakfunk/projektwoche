/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tableModels;

/**
 *
 * @author bakfunk
 */
public class Vorgang {

    private int Vorgangsnr;
    private String Vorgangsart;
    private int Fk_Status;
    private int Fk_Geschaeftspartner;
    private int Fk_Adresse;
    private int Fk_Zahlung;
    private int Fk_Mitarbeiter;

    public Vorgang() {
    }

    public Vorgang(int Vorgangsnr, String Vorgangsart, int Fk_Stauts, int Fk_Geschaeftspartner, int Fk_Adresse, int Fk_Zahlung, int Fk_Mitarbeiter) {
        this.Vorgangsnr = Vorgangsnr;
        this.Vorgangsart = Vorgangsart;
        this.Fk_Status = Fk_Stauts;
        this.Fk_Geschaeftspartner = Fk_Geschaeftspartner;
        this.Fk_Adresse = Fk_Adresse;
        this.Fk_Zahlung = Fk_Zahlung;
        this.Fk_Mitarbeiter = Fk_Mitarbeiter;
    }
     public Object[] getObjectArray() {
        Object[] data = new Object[] {
            Vorgangsnr,
            Vorgangsart,
            Fk_Status,
            Fk_Geschaeftspartner,
            Fk_Adresse,
            Fk_Zahlung,
            Fk_Mitarbeiter
        };
        
        return data;
    }

    public int getVorgangsnr() {
        return Vorgangsnr;
    }

    public void setVorgangsnr(int Vorgangsnr) {
        this.Vorgangsnr = Vorgangsnr;
    }

    public String getVorgangsart() {
        return Vorgangsart;
    }

    public void setVorgangsart(String Vorgangsart) {
        this.Vorgangsart = Vorgangsart;
    }

    public int getFk_Status() {
        return Fk_Status;
    }

    public void setFk_Status(int Fk_Status) {
        this.Fk_Status = Fk_Status;
    }

    public int getFk_Geschaeftspartner() {
        return Fk_Geschaeftspartner;
    }

    public void setFk_Geschaeftspartner(int Fk_Geschaeftspartner) {
        this.Fk_Geschaeftspartner = Fk_Geschaeftspartner;
    }

    public int getFk_Adresse() {
        return Fk_Adresse;
    }

    public void setFk_Adresse(int Fk_Adresse) {
        this.Fk_Adresse = Fk_Adresse;
    }

    public int getFk_Zahlung() {
        return Fk_Zahlung;
    }

    public void setFk_Zahlung(int Fk_Zahlung) {
        this.Fk_Zahlung = Fk_Zahlung;
    }

    public int getFk_Mitarbeiter() {
        return Fk_Mitarbeiter;
    }

    public void setFk_Mitarbeiter(int Fk_Mitarbeiter) {
        this.Fk_Mitarbeiter = Fk_Mitarbeiter;
    }

}
