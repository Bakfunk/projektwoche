/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Testing;

import database.DBZugriff;
import java.sql.ResultSet;
import java.sql.SQLException;
import tableModels.Artikel;

/**
 *
 * @author TagmouzSa
 */
public class TableManipulation {
    
    
    
    
    
    
    public static void main(String[] args) throws SQLException {
        DBZugriff db = new DBZugriff();
        db.oeffneDBMSQL();
        //lesen(db);
//        suchenUndlesenArtikel(db, "00160");
        db.login("database", "g7java");
        db.schliesseDB();
        
    }
    
    
    
   public static void lesen(DBZugriff db) throws SQLException{
    String sqlquery = "SELECT * FROM artikel";
    
    ResultSet rs = db.lesen(sqlquery);
  
    
    while(rs.next()){
         String aNr = rs.getString("ArtikelNr");
         String Name = rs.getString("Name");
         String beschreibung = rs.getString("Beschreibung");
         //int bestand = rs.getInt("Bestand");
         double preis = rs.getDouble("Bruttopreis");
         double nettopreis = rs.getDouble("Nettopreis");
         int menge = rs.getInt("Menge");
         int Biosiegel = rs.getInt("Biosiegel");
         int Steuersatz = rs.getInt("Steuersatz");
         int eanNummer = rs.getInt("EAN Nummer");
         int fairtrade = rs.getInt("Fairtrade");
         int Fk_Artikelgruppe = rs.getInt("Fk_Artikelgruppe");
         System.out.println("Erfolgreiche Abfrage wird nun angezeigt: ");
         System.out.println(aNr+" "+Name+" "+beschreibung+" "+preis+" "+nettopreis+" "+menge+" "+
                             Biosiegel+" "+Steuersatz+" "+eanNummer+" "+fairtrade+" "+Fk_Artikelgruppe);
       
    }
   }
   
   
   public static void schreiben(DBZugriff db) throws SQLException{
       String sqlquery = "INSERT INTO testing (Name, zahl) VALUES ('drölf',2)";
       db.einfuegen(sqlquery);
       
       
   }
   
   public static void suchenUndlesenArtikel(DBZugriff db, String pAnr) throws SQLException{
         String sqlquery = "SELECT * FROM artikel WHERE ArtikelNr = '"+pAnr+"'";
        
        db.oeffneDB();
        ResultSet rs = db.lesen(sqlquery);
        
        System.out.println("Abfrage wurde gestartet.");
        while(rs.next()){
          
         String aNr = rs.getString("ArtikelNr");
         String Name = rs.getString("Name");
         String beschreibung = rs.getString("Beschreibung");
         //int bestand = rs.getInt("Bestand");
         double preis = rs.getDouble("Bruttopreis");
         double nettopreis = rs.getDouble("Nettopreis");
         int menge = rs.getInt("Menge");
         int Biosiegel = rs.getInt("Biosiegel");
         int Steuersatz = rs.getInt("Steuersatz");
         int eanNummer = rs.getInt("EAN Nummer");
         int fairtrade = rs.getInt("Fairtrade");
         int Fk_Artikelgruppe = rs.getInt("Fk_Artikelgruppe");
         System.out.println("Erfolgreiche Abfrage wird nun angezeigt: ");
         System.out.println(aNr+" "+Name+" "+beschreibung+" "+preis+" "+nettopreis+" "+menge+" "+
                             Biosiegel+" "+Steuersatz+" "+eanNummer+" "+fairtrade+" "+Fk_Artikelgruppe);
        }
        
      
   
   }
   
   public void schreibenMitarbeiter(DBZugriff db,String Personalnummer,String Vorname,String Name,int GebDat,int Fk_Abteilung) throws SQLException{
   String sqlquery = "INSERT INTO Mitarbeiter (Personalnummer, Vorname, Name, GebDat, Fk_Abteilung) VALUES ('Personalnummer', 'Vorname','Name', GebDat, Fk_Abteilung)";
   db.einfuegen(sqlquery);
   
   
   }
}
