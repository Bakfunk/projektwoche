/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.ResultSet;
import java.sql.SQLException;
import tableModels.Vorgang;

/**
 *
 * @author bakfunk
 */
public class VorgangDB {

    private DBZugriff derDBZugriff;

    public String lesenVorgang() throws Throwable {

        String sqlquery = "SELECT * FROM vorgang";

        return sqlquery;
    }

    public void einfuegenVorgang(Vorgang pVorgang) throws SQLException {
        DBZugriff db = new DBZugriff();
        String sqlquery = "INSERT INTO vorgang(Vorgangsnr, Vorgangsart, Fk_Status, Fk_Geschaeftspartner, Fk_Adresse,"
                + " Fk_Zahlung, Fk_Mitarbeiter)"
                + "VALUES ('" + pVorgang.getVorgangsnr() + "','" + pVorgang.getVorgangsart() + "','" + pVorgang.getFk_Status() + "', '"
                + pVorgang.getFk_Status() + "', '" + pVorgang.getFk_Geschaeftspartner() + "','" + pVorgang.getFk_Zahlung() + "','" + pVorgang.getFk_Mitarbeiter() + "')";
        db.oeffneDB();
        db.einfuegen(sqlquery);
        db.schliesseDB();
    }

    public Vorgang sucheVorgang(String pVorgangsnr) throws SQLException {
        DBZugriff db = new DBZugriff();
        String sqlquery = "SELECT * FROM " + Vorgang.class.getName() + " WHERE Vorgangsnr = '" + pVorgangsnr + "' ";

        db.oeffneDB();
        ResultSet rs = db.lesen(pVorgangsnr);

        Vorgang vorgang = null;

        while (rs.next()) {

            int Vorgangsnr = rs.getInt("Vorgangsnr");
            String Vorgangsart = rs.getString("Vorgangsart");
            int Fk_Status = rs.getInt("Fk_Status");
            int Fk_Geschaeftspartner = rs.getInt("Fk_Geschaeftspartner");
            int Fk_Adresse = rs.getInt("Fk_Adresse");
            int Fk_Zahlung = rs.getInt("Fk_Zahlung");
            int Fk_Mitarbeiter = rs.getInt("Fk_Mitarbeiter");

            vorgang = new Vorgang(Vorgangsnr, Vorgangsart, Fk_Status, Fk_Geschaeftspartner, Fk_Adresse, Fk_Zahlung, Fk_Mitarbeiter);

        }

        return vorgang;
    }

    public void updateVorgang(DBZugriff db, Vorgang vorgang, String altVorgangsnr) throws SQLException {
        String sqlquery = "UPDATE FROM vorgang"
                + "SET Vorgangsnr = '" + vorgang.getVorgangsnr() + "' Vorgangsart = '" + vorgang.getVorgangsart() + "' Fk_Status = '" + vorgang.getFk_Status()
                + " Fk_Geschaeftspartner = '" + vorgang.getFk_Geschaeftspartner() + "' Fk_Adresse = '" + vorgang.getFk_Adresse() + "' Fk_Zahlung = '" + vorgang.getFk_Zahlung()
                + " Fk_Mitarbeiter  = '" + vorgang.getFk_Mitarbeiter()
                + " WHERE Vorgangsnr = '" + altVorgangsnr + "' ";

        db.oeffneDB();
        ResultSet rs = db.lesen(sqlquery);
        db.schliesseDB();

    }

    public void loeschenArtikel(DBZugriff db, String Vorgangsnr) throws SQLException {
        String sqlquery = "DELETE FROM vorgang WHERE Vorgangsnr = '" + Vorgangsnr + "' ";

        db.oeffneDB();
        ResultSet rs = db.lesen(sqlquery);
        db.schliesseDB();

    }

}
