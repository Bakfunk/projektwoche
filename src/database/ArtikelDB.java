package database;

import java.sql.ResultSet;
import java.sql.SQLException;
import tableModels.Artikel;



public class ArtikelDB {

public ArtikelDB() {

}
    
    /**
     * Neuer Artikel wird erstellt und in die Datenbankgespeichert
     * 
     * @param pArtikel Objekt Artikel mit werten
     * @throws Throwable 
     */
    public void einfuegenArtikel(Artikel pArtikel) throws Throwable {
        DBZugriff db = new DBZugriff();
        String sqlquery = "INSERT INTO artikel VALUES ('" + pArtikel.getaNr() + "','" + pArtikel.getName() + "','" + pArtikel.getBeschreibung() + "', '"
                + pArtikel.getPreis() + "', '" + pArtikel.getNettopreis() + "','" + pArtikel.getMenge() + "','" + pArtikel.getBiosiegel() + "','"
                + pArtikel.getSteuersatz() + "','" + pArtikel.getVE() + "','" + pArtikel.getEANNummer() + "','" + pArtikel.getFairtrade() + "','" + pArtikel.getFk_Erzeuger() + "','" + pArtikel.getFk_Artikelgruppe() + "');";
        db.oeffneDB();
        db.einfuegen(sqlquery);
        db.schliesseDB();

    }

    /**
     * Funktion übergibt ein String mit SQL Statement für das auslesen einer Tabelle
     * 
     * @return squelquery wird übergeben
     * @throws Throwable 
     */
    public String lesenArtikel() throws Throwable {
//        DBZugriff db = new DBZugriff();
        String sqlquery = "SELECT * FROM artikel";

//        db.oeffneDB();
//        ResultSet rs = db.lesen(sqlquery);

        
//        while (rs.next()) {
//
//            String aNr = rs.getString("ArtikelNr");
//            String Name = rs.getString("Name");
//            String beschreibung = rs.getString("Beschreibung");
//            //int bestand = rs.getInt("Bestand");
//            double preis = rs.getDouble("Bruttopreis");
//            double nettopreis = rs.getDouble("Nettopreis");
//            int menge = rs.getInt("Menge");
//            int Biosiegel = rs.getInt("Biosiegel");
//            int Steuersatz = rs.getInt("Steuersatz");
//            int eanNummer = rs.getInt("EAN Nummer");
//            int fairtrade = rs.getInt("Fairtrade");
//            int Fk_Artikelgruppe = rs.getInt("Fk_Artikelgruppe");
//
//            artikel = new Artikel(aNr, Name, beschreibung, preis, nettopreis, menge, Biosiegel, Steuersatz, eanNummer, fairtrade, Fk_Artikelgruppe);
//
//        }
//        db.schliesseDB();
//        return artikel;
          return sqlquery;
    }
    
    /**
     * Funktion die nach einem Artikel in der Datenbank sucht
     * 
     * @param pAnr ein als String eindeutige Artikelnummer die für die Suche erforderlich ist
     * @return
     * @throws Throwable 
     */
    public Artikel sucheArtikel(String pAnr) throws Throwable {
        DBZugriff db = new DBZugriff();
        String sqlquery = "SELECT * FROM artikel WHERE ArtikelNr = '" + pAnr + "' ";

        db.oeffneDB();
        ResultSet rs = db.lesen(sqlquery); 

        Artikel artikel = null;
        while (rs.next()) {

            String aNr = rs.getString("ArtikelNr");
            String Name = rs.getString("Name");
            String beschreibung = rs.getString("Beschreibung");
            double preis = rs.getDouble("Bruttopreis");
            double nettopreis = rs.getDouble("Nettopreis");
            int menge = rs.getInt("Menge");
            int Biosiegel = rs.getInt("Biosiegel");
            int Steuersatz = rs.getInt("Steuersatz");
            int VE = rs.getInt("VE");
            long eanNummer = rs.getLong("EANNummer");
            int fairtrade = rs.getInt("Fairtrade");
            int Fk_Erzeuger = rs.getInt("Fk_Erzeuger");
            int Fk_Artikelgruppe = rs.getInt("Fk_Artikelgruppe");

            artikel = new Artikel(aNr, Name, beschreibung, preis, nettopreis, menge, Biosiegel, Steuersatz, VE, eanNummer, fairtrade, Fk_Erzeuger, Fk_Artikelgruppe);
            System.out.println(artikel);

        }
        db.schliesseDB();
        return artikel;
    }
     
    /**
     * Funktion die ein bisherhigen Artikel ändert
     * 
     * @param db Datenbankzugriff
     * @param artikel Objekt Artikel mit neuen werten
     * @param arNr  alte Artikelnummer nach der gesucht wird bevor sie geändert werden kann.
     * @throws SQLException 
     */
     public void updateArtikel(DBZugriff db, Artikel artikel,String arNr) throws SQLException {
        String sqlquery = "UPDATE artikel "+
                          "SET ArtikelNr = '"+artikel.getaNr()+"',Name = '"+artikel.getName()+"', Beschreibung = '"+artikel.getBeschreibung()+"',"
                          +" Bruttopreis = '"+artikel.getPreis()+"', Nettopreis= '"+artikel.getNettopreis()+"', Menge='"+artikel.getMenge()+"', Biosiegel='"+artikel.getBiosiegel()+"',"
                          +" Steuersatz ='"+artikel.getSteuersatz()+"', VE='"+artikel.getVE()+"',EANNummer='"+artikel.getEANNummer()+"', Fairtrade='"+artikel.getFairtrade()+"', Fk_Erzeuger='"+artikel.getFk_Erzeuger()+"' ,Fk_Artikelgruppe='"+artikel.getFk_Artikelgruppe()+"'"
                          +" WHERE ArtikelNr = '" + arNr +"' ";

        db.oeffneDB();
        ResultSet rs = db.lesen(sqlquery);
        db.schliesseDB();

    }
     
    /**
     * Funktion die einen Artikel löscht
     * @param db Datenbankzugriff
     * @param pAnr Artikelnummer. Ein String um den Artikel zu suchen welches gelöscht wird.
     * @throws SQLException 
     */
    public void loeschenArtikel(DBZugriff db, String pAnr) throws SQLException {
        String sqlquery = "DELETE FROM artikel WHERE ArtikelNr = '" + pAnr + "' ";

        db.oeffneDB();
        ResultSet rs = db.lesen(sqlquery);
        db.schliesseDB();

    }

}


