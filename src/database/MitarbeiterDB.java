/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import tableModels.Mitarbeiter;

/**
 *
 * @author bakfunk
 */
public class MitarbeiterDB {

    public MitarbeiterDB() {
    }
    
    
     public void einfuegenMitarbeiter(Mitarbeiter pMitarbeiter) throws Throwable {
        DBZugriff db = new DBZugriff();
        String sqlquery = "INSERT INTO mitarbeiter (Personalnummer, Vornamme, Name, GebDat, Fk_Abteilung)"
     
                + "VALUES ('" + pMitarbeiter.getPersonalnummer() + "','" + pMitarbeiter.getVorname()+ "','" + pMitarbeiter.getGebDat()+ "', '"
                + pMitarbeiter.getFk_Abteilung()+ "')";
        db.oeffneDB();
        db.einfuegen(sqlquery);
        db.schliesseDB();

    }

    
    /**
     * Funktion übergibt ein String mit SQL Statement für das auslesen einer Tabelle
     * 
     * @return squelquery wird übergeben
     * @throws Throwable 
     */
    public String lesenMitarbeiter() throws Throwable {
//        DBZugriff db = new DBZugriff();
        String sqlquery = "SELECT * FROM mitarbeiter";

//        db.oeffneDB();
//        ResultSet rs = db.lesen(sqlquery);

        
//        while (rs.next()) {
//
//            String aNr = rs.getString("ArtikelNr");
//            String Name = rs.getString("Name");
//            String beschreibung = rs.getString("Beschreibung");
//            //int bestand = rs.getInt("Bestand");
//            double preis = rs.getDouble("Bruttopreis");
//            double nettopreis = rs.getDouble("Nettopreis");
//            int menge = rs.getInt("Menge");
//            int Biosiegel = rs.getInt("Biosiegel");
//            int Steuersatz = rs.getInt("Steuersatz");
//            int eanNummer = rs.getInt("EAN Nummer");
//            int fairtrade = rs.getInt("Fairtrade");
//            int Fk_Artikelgruppe = rs.getInt("Fk_Artikelgruppe");
//
//            artikel = new Artikel(aNr, Name, beschreibung, preis, nettopreis, menge, Biosiegel, Steuersatz, eanNummer, fairtrade, Fk_Artikelgruppe);
//
//        }
//        db.schliesseDB();
//        return artikel;
          return sqlquery;
    }
    
    
    
    /**
     * Funktion zur Suche eines Mitarbeiters in der Datenbank
     * @param pPersonalnummer Personalnummer für die Suche nach einem Mitarbeiter
     * @return Objekt Mitarbeiter mit dementsprechenden werten.
     * @throws Throwable 
     */
    public Mitarbeiter sucheMitarbeiter(String pPersonalnummer) throws Throwable {
        DBZugriff db = new DBZugriff();
        String sqlquery = "SELECT * FROM mitarbeiter WHERE Personalnummer = '" + pPersonalnummer + "' ";

        db.oeffneDB();
        ResultSet rs = db.lesen(sqlquery);

        Mitarbeiter mitarbeiter = null;
        while (rs.next()) {

            String aNr = rs.getString("Personalnummer");
            String Vorname = rs.getString("Vorname");
            String beschreibung = rs.getString("Name");
            Date GebDat = rs.getDate("GebDat");
            int Fk_Abteilung = rs.getInt("Fk_Abteilung");
            

            mitarbeiter = new Mitarbeiter(pPersonalnummer, Vorname, Vorname, GebDat, Fk_Abteilung);
        }
        db.schliesseDB();
        return mitarbeiter;
    }
    
    
    /**
     * Funktion die ein bisherhigen Artikel ändert
     * 
     * @param db Datenbankzugriff
     * @param mitarbeiter Objekt Mitarbeiter mit neuen werten
     * @param arNr  alte Personalnummer nach der gesucht wird bevor sie geändert werden kann.
     * @throws SQLException 
     */
     public void updateMitarbeiter(DBZugriff db, Mitarbeiter mitarbeiter,String altPersonalnummer) throws SQLException {
        String sqlquery = "UPDATE FROM Personalnummer"+
                          "SET Personalnummer = '"+mitarbeiter.getPersonalnummer()
                          +"' Vorname = '"+mitarbeiter.getVorname()+"' Name = '"+mitarbeiter.getGebDat()
                          +"' Fk_Abteilung = '"+mitarbeiter.getFk_Abteilung()+"'"
                          +" WHERE Personalnummer = '" + altPersonalnummer +"' ";

        db.oeffneDB();
        ResultSet rs = db.lesen(sqlquery);
        db.schliesseDB();

    }
    
     
    /**
     * Funktion die einen Artikel löscht
     * @param db Datenbankzugriff
     * @param pAnr Artikelnummer. Ein String um den Artikel zu suchen welches gelöscht wird.
     * @throws SQLException 
     */
    public void loeschenMitarbeiter(DBZugriff db, String Personalnummer) throws SQLException {
        String sqlquery = "DELETE FROM mitarbeiter WHERE Personalnummer = '" + Personalnummer + "' ";

        db.oeffneDB();
        ResultSet rs = db.lesen(sqlquery);
        db.schliesseDB();

    }
    
}
