/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import database.DBZugriff;
import database.MitarbeiterDB;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import tableModels.Mitarbeiter;

/**
 *
 * @author Simons Laptop
 */
public class WorkerFrame extends javax.swing.JFrame {

    MitarbeiterDB mitarbeiterDb                     = new MitarbeiterDB();
    DBZugriff dbZugriff                             = new DBZugriff();
    DefaultTableModel tableModel                    = new DefaultTableModel();
    public WorkerFrame() {        
         try {
            initComponents();            
            tableModel.addColumn("Personalnummer");
            tableModel.addColumn("Vorname");
            tableModel.addColumn("Name");
            tableModel.addColumn("Geburtsdatum");
            tableModel.addColumn("Fk_Abteilung");
            dbZugriff.oeffneDB();
            ArrayList<Mitarbeiter> list = GetDataFromQuery(mitarbeiterDb.lesenMitarbeiter());
             for (int i = 0; i < list.size(); i++) {
                 tableModel.addRow(list.get(i).getObjectArray());
             }            
            tblWorker.setModel(tableModel);
            dbZugriff.schliesseDB();
        }
        catch (SQLException fehler) {
         fehler.printStackTrace();
        } catch (Throwable ex) {
            Logger.getLogger(WorkerFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlWorker = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblWorker = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        pnlWorker.setBackground(new java.awt.Color(200, 215, 230));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 48)); // NOI18N
        jLabel1.setText("Mitarbeiterverwaltung");

        tblWorker.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(tblWorker);

        javax.swing.GroupLayout pnlWorkerLayout = new javax.swing.GroupLayout(pnlWorker);
        pnlWorker.setLayout(pnlWorkerLayout);
        pnlWorkerLayout.setHorizontalGroup(
            pnlWorkerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlWorkerLayout.createSequentialGroup()
                .addContainerGap(350, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(383, 383, 383))
            .addGroup(pnlWorkerLayout.createSequentialGroup()
                .addGap(61, 61, 61)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 1081, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlWorkerLayout.setVerticalGroup(
            pnlWorkerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlWorkerLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 160, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 538, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlWorker, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlWorker, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(WorkerFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(WorkerFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(WorkerFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(WorkerFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new WorkerFrame().setVisible(true);
            }
        });
    }
    
    private ArrayList GetDataFromQuery(String sqlQuery) {
        ArrayList<Mitarbeiter> list = new ArrayList();
        Mitarbeiter mitarbeiter;
        try{
            ResultSet rs = dbZugriff.lesen(sqlQuery);
            while(rs.next()) {
                String personalNummer = rs.getString("Personalnummer");
                String Vorname = rs.getString("Vorname");
                String Name = rs.getString("Name");
                Date GebDat = rs.getDate("GebDat");
                int Fk_Abteilung = rs.getInt("Fk_Abteilung");
                
                mitarbeiter = new Mitarbeiter(personalNummer, Vorname, Name, GebDat, Fk_Abteilung);
                list.add(mitarbeiter);
            }
        }
        catch(Throwable error) {
            error.printStackTrace();
        }
        return list;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPanel pnlWorker;
    private javax.swing.JTable tblWorker;
    // End of variables declaration//GEN-END:variables
}
