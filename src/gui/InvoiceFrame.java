/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import database.DBZugriff;
import database.VorgangDB;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;
import tableModels.Vorgang;

/**
 *
 * @author Simons Laptop
 */
public class InvoiceFrame extends javax.swing.JFrame {

    VorgangDB vorgangDb                     = new VorgangDB();
    DBZugriff dbZugriff                     = new DBZugriff();
    
    public InvoiceFrame() {
         try{
            initComponents();
            DefaultTableModel tableModel = new DefaultTableModel();
            tableModel.addColumn("VorgangsNr");
            tableModel.addColumn("Vorgangsart");
            tableModel.addColumn("Fk_Status");
            tableModel.addColumn("Fk_Geschaeftspartner");
            tableModel.addColumn("Fk_Adresse");
            tableModel.addColumn("Fk_Zahlung");
            tableModel.addColumn("Fk_Mitarbeiter");
            dbZugriff.oeffneDB(); 
            ArrayList<Vorgang> list = GetDataFromQuery(vorgangDb.lesenVorgang());
             for (int i = 0; i < list.size(); i++) {
                 tableModel.addRow(list.get(i).getObjectArray());
             }

            tblInvoice.setModel(tableModel);
            dbZugriff.schliesseDB();
        }
        catch (Throwable fehler) {
         fehler.printStackTrace();
      } 
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblInvoice = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(200, 215, 230));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 48)); // NOI18N
        jLabel1.setText("Vorgangsverwaltung");

        tblInvoice.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblInvoice);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(400, 400, 400)
                        .addComponent(jLabel1))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(42, 42, 42)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(38, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 118, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 600, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(InvoiceFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(InvoiceFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(InvoiceFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(InvoiceFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new InvoiceFrame().setVisible(true);
            }
        });
    }
    
     private ArrayList GetDataFromQuery(String sqlQuery) {
        ArrayList<Vorgang> list = new ArrayList();
        Vorgang vorgang;
        try{
            ResultSet rs = dbZugriff.lesen(sqlQuery);
            while(rs.next()) {
                int Vorgangsnummer = rs.getInt("Vorgangsnr");
                String Vorgangsart = rs.getString("Vorgangsart");
                int Fk_Status = rs.getInt("Fk_Status");
                int Fk_Geschaeftspartner = rs.getInt("Fk_Geschaeftspartner");
                int Fk_Adresse = rs.getInt("Fk_Adresse");
                int Fk_Zahlung = rs.getInt("Fk_Zahlung");
                int Fk_Mitarbeiter = rs.getInt("Fk_Mitarbeiter");
                vorgang = new Vorgang(Vorgangsnummer, Vorgangsart, Fk_Status, Fk_Geschaeftspartner, Fk_Adresse, Fk_Zahlung, Fk_Mitarbeiter);
                list.add(vorgang);
            }
        }
        catch(Throwable error) {
            error.printStackTrace();
        }
        return list;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblInvoice;
    // End of variables declaration//GEN-END:variables
}
